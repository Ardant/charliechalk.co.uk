<!DOCTYPE html><html><head><title>Professional Chalkboard Artist - Charlie Chalk</title><meta charset="utf-8"><meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" /><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/><script src="http://code.jquery.com/jquery-latest.min.js"></script>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta name="format-detection" content="telephone=no">
<link href="https://fonts.googleapis.com/css?family=Handlee" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/css/bootslate.css">
<link rel="stylesheet" type="text/css" href="/css/fresco.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/responsive.css">
<link type="text/plain" rel="author" href="/humans.txt" />
<link href="css/bootslate.css" rel="stylesheet" type="text/css">
<meta name="description" content="Charlie Chalk is a Midlands-based chalkboard artist with over 20 years' experience.">
</head>
<body>
<div id="chalkboard"></div>
<div class="container-fluid">
    <div class="row">
       <div id="copy" class="col-sm-4 col-md-5">
            <header>
                <h1>Charlie Chalk</h1>
                <h2>(The Original Since 1995)</h2>
            </header>
            <section>
                <p class="caps">Professional chalkboard<br/>~ artist ~</p>
                <p>To the Pub, Restaurant and Retail Trade</p>
                <p class="caps">Based in the Midlands<br/>~ nationwide service ~</p>
                <p>Mobile : <a href="tel:07890 921220">07890 921220</a><br/>
                Home : <a href="tel:01902 270477">01902 270477</a></p>
                <p>Email : <a href="mailto:charlie@charliechalk.co.uk">charlie@charliechalk.co.uk</a></p>
                
                <div id="gallery">
                	<h2>Gallery Examples</h2>
                </div>
            </section>
       </div>
       <div class="col-sm-8 col-md-7 grid col-sm-offset-4 col-md-offset-5">
       		<?php $count = 1; $files = glob('img/gallery/*.{jpg,png,gif}', GLOB_BRACE);
			foreach($files as $file) {
			/*
					$desired_width = 400;
					$source_image = imagecreatefromjpeg($file);
					$width = imagesx($source_image);
					$height = imagesy($source_image);
					$desired_height = floor($height * ($desired_width / $width));
					$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
					imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
					imagejpeg($virtual_image, "thumbs/".$file, 100);
				*/
				?>
				<div class="griditem <?php if ($file == "img/gallery/20.jpg" || $file == "img/gallery/x.jpg" || $file == "img/gallery/x.jpg") { echo "griditem-wide";}?>">
                	<a data-fresco-group="signs" class="fresco" href="<?php echo $file;?>"><img src="<?php echo "/thumbs/".$file;?>" alt="Example Chalkboard <?php echo $count; $count++?>"/></a>
                </div>
			<?php } ?>
       </div>
	</div><!--row-->    
</div><!--container-->
<footer id="footer">
    &copy; <?php echo date("Y") ?>  - 
</footer>
<script src="//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="/js/packery.js"></script>
<script src="/js/fresco.js"></script>
<script src="/js/ardant.js"></script>
</body>
</html>